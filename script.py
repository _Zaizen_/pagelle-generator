out_completo = ""
out_completo_file = open('output.html', "w")
linee_voti = 0


def word_find(line,words):
    #print(line)
    #print(line.strip().split())
    #print(set(words))
    #print(list(set(line.strip().split()) & set(words)))
    return list(set(line.strip().split()) & set(words))

def fa_cose(values):
    sum = values["SPIEGAZIONI_VOTO"] + values["COERENZA_VOTO"] + values["RAPPORTO_VOTO"] + values["VALUTAZIONI_VOTO"] + values["COMPORTAMENTO_VOTO"]
    values["MEDIA_VOTO"] = sum/5
    file = 'html_da_compilare.html'
    output = ""
    out = open(values["NOMEINSEGNANTE"] + '.html', "w")

    words = ("NOMEINSEGNANTE", "SPIEGAZIONI_VOTO", "COERENZA_VOTO", "RAPPORTO_VOTO", "VALUTAZIONI_VOTO", "COMPORTAMENTO_VOTO", "MEDIA_VOTO")
    with open(file) as f:

        #content = f.readlines()
        #print(content[210])
        #word_find(content[210],words)
        for i,x in enumerate(f, start=1):
            common = word_find(x,words)
            if common:
                #print(x.replace(common[0], values[common[0]]))

                output = output + x.replace(common[0], str(values[common[0]]))
                #print( i, "".join(common))
            else:
                #print(x)
                output = output + x
    #print(output)
    out.write(output)
    out.close()
    return output

while True:
    print("Cosa vuoi fare?")
    print("1) Creare una nuova pagella")
    print("2) Importare dal file voti.txt")
    print("3) Uscire")
    scelta = int(input("Inserisci il numero relativo alla tua scelta: "))
    if scelta == 3:
        break
    if scelta != 1 and scelta != 2:
        print("Valore non valido, ritenta")
        continue
    values = {}
    if scelta == 1:
        nomeins = input("Inserisci nome e cognome dell'insegnante: ")
        values["NOMEINSEGNANTE"] = nomeins.upper()
        values["SPIEGAZIONI_VOTO"] = int(input("Inserisci il suo voto nelle spiegazioni: "))
        values["COERENZA_VOTO"] = int(input("Inserisci il suo voto nella coerenza: "))
        values["RAPPORTO_VOTO"] = int(input("Inserisci il suo voto nel rapporto con gli studenti: "))
        values["VALUTAZIONI_VOTO"] = int(input("Inserisci il suo voto nelle valutazioni: "))
        values["COMPORTAMENTO_VOTO"] = int(input("Inserisci il suo voto in comportamento: "))
        output = fa_cose(values)
        out_completo = out_completo + output + "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>"
    else:
        file_voti = open("voti.txt", "r")
        righe = file_voti.readlines()
        while linee_voti < len(righe):
            values["NOMEINSEGNANTE"] = righe[linee_voti].rstrip().upper()
            linee_voti = linee_voti + 1
            values["SPIEGAZIONI_VOTO"] = int(righe[linee_voti])
            linee_voti = linee_voti + 1
            values["COERENZA_VOTO"] = int(righe[linee_voti])
            linee_voti = linee_voti + 1
            values["RAPPORTO_VOTO"] = int(righe[linee_voti])
            linee_voti = linee_voti + 1
            values["VALUTAZIONI_VOTO"] = int(righe[linee_voti])
            linee_voti = linee_voti + 1
            values["COMPORTAMENTO_VOTO"] = int(righe[linee_voti])
            linee_voti = linee_voti + 1
            output = fa_cose(values)
            out_completo = out_completo + output + "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>"


out_completo_file.write(out_completo)
out_completo_file.close()
